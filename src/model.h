//
// Created by Christopher on 11/16/20.
//

#ifndef MUSICAL_SOCKETS_MODEL_H
#define MUSICAL_SOCKETS_MODEL_H

#include <iostream>
#include <pthread.h>
#include <mutex>
#include <condition_variable>
#include <array>

#define nKids 5

std::mutex m;
std::condition_variable cv1;
std::condition_variable cv2;

struct Model{

    int chairArray[nKids] = {-1,-1,-1,-1,-1};
    int *pChairArray = chairArray;
    int nChairs = (nKids-1);
    int nMarching = nKids;

    //Model Constructor
    Model(){
        std::cout << "Model Constructor" << std::endl;
    }
    //Model Destructor
    ~Model(){
        std::cout << "Model Destructor" << std::endl;
    }

};

#endif //MUSICAL_SOCKETS_MODEL_H
