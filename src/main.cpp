/* Systems Programming
 * University of New Haven
 * Created by Christopher Dowd
 * 11/16/2020
 */

#include <iostream>
#include <pthread.h>
#include "kid.h"
#include "model.h"
#include <csignal>
#include <sstream>

#define NUM_THREADS 5

pthread_mutex_t mtx;

//Generic global function to start thread
void* startThread(void* kid){
    Kid* k = (Kid*) kid;
    k->play();
    std::cout << "start thread" << std::endl;
    return nullptr;
}

int main() {

    //Display message when main thread begins
    std::cout <<"main thread starting..." << std::endl;
    pthread_t threads[NUM_THREADS];
    std::ostringstream ss;
    Model *m;
    Kid k(true, 1, 7);
    Kid km(m, 5);

    //Create all kid threads using startThread function
    for(long t = 0; t < NUM_THREADS; t++){
        pthread_mutex_lock(&mtx);
        ss << "main: creating kid # " << t << std::endl;
        std::cout << ss.str();
        ss.str("");
       pthread_create(&threads[t], 0 , startThread, (void*) t);
       pthread_mutex_unlock(&mtx);
    }
    for(int t= 0; t < NUM_THREADS; t++){

       pthread_join(threads[t], NULL);

    }

    return 0;
}
