//
// Created by Christopher on 11/16/20.
//

#ifndef MUSICAL_SOCKETS_KID_H
#define MUSICAL_SOCKETS_KID_H

#include <iostream>
#include <pthread.h>
#include <mutex>
#include "model.h"



class Kid{
private:
    int kStart = 0;
    int kidID = 0;
    int tid;
    int wantSeat = 0;
    int seatNumber = 0;
    Model *model_pointer;
    bool sit = false;
    pthread_t thread[nKids];

public:
    void play(){
        std::cout << "Kids play has begun" << std::endl;
    }
    Kid(Model* m, int idNum){
        this->model_pointer = m;
        this->kidID = idNum;
        std::cout << "kid model constructor called" << std::endl;
    }
    Kid(bool s, int kID, int tid){
        this->sit = s;
        this->kidID = kID;
        this->tid = tid;
        std::cout << "Kid bool constructor called" << std::endl;
    }

    //Kid Constructor
    Kid(){
        std::cout << "Kid constructor" << std::endl;
    }
    //Kid Destructor
    ~Kid(){
        std::cout << "Kid destructor" << std::endl;
    }

};

#endif //MUSICAL_SOCKETS_KID_H
